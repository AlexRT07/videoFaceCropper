# Face Detector and Cropper

This repository has a code programmed in C++ with OpenCV 2.4.9.

The idea behind this code is to detect all the faces in the frames of a video, and crop and save them in a folder. The final intention is to build an database of known faces to train a convolutional neural network to recognize some people.

The code has commented documentation in spanish.

## Requirements

You'll need the following to compile this code:

* GNU/Linux distribution: The code has several POSIX commands that works only under Linux. This code has been programmed in Ubuntu 16.04 LTS.

* OpenCV. This code has been tested with OpenCV 2.4.9

* Build-essential. If you don't know if you have this, just open a terminal and type: "sudo apt-get install build-essential".

## How to build

Just open a terminal and type "make". And it will be done!

## Use

Put into the folder a video file in a format supported by OpenCV, and type "./faces PutYourVideoHere". It will take a while to end, so, be patient.

## Acknowledgment
I want to thank to **Instituto Tecnológico de Calkiní en el Estado de Campeche - ITESCAM** and **Dr. José Luis Lira Turriza**, career coordinator, for the idea and support in the elaboration of this project.
