faces: faces.cpp

	g++ -o faces faces.cpp `pkg-config --cflags --libs opencv` -std=c++11

clean:
	rm faces
