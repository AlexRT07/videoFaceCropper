/*
 * Programa que toma frames de un vídeo, reconoce las caras mediante el método
 * de características haar usando imágenes integrales que viene por defecto 
 * en OpenCV y guarda todas las imágenes de caras reconocidas en un directorio.
 * 
 * El programa puede ser distribuido libremente recordando citar al autor.
 * No me hago responsable del uso de este código por terceros
 *
 * Ing. Alex Antonio Turriza Suárez, 2018
*/

#include <opencv2/core/core.hpp> //OpenCv2
#include <opencv2/highgui/highgui.hpp> //OpenCv2
#include "opencv2/objdetect/objdetect.hpp" //OpenCv2
#include "opencv2/imgproc/imgproc.hpp" //OpenCv2
#include <iostream> //IO estándar de C++
#include <sstream> //Para nombrar imágenes a guardar
#include <vector> //Para guardar caras detectadas
#include <sys/stat.h> //Se usa con mkdir
#include <sys/types.h> //Se usa con mkdir
#include <errno.h> //Para verificar qué error del sistema se tiene

#define FOLDERNAME "caras1"

using namespace std;

void cutNSave(cv::Mat * frame, cv::Rect * face, int cont, int p)
{
	ostringstream name;
	cv::Mat prev = * frame;
	cv::Mat temporal = prev(*face); //Guarda el contenido del rectángulo en una matriz temporal
	name << FOLDERNAME << "/" << "F_" << cont << "_" << p << ".jpg";
	cv::imwrite(name.str(), temporal);
	temporal.release();
	prev.release();
}

int main(int argc, char** argv)
{
	/*Fase de verificaciones*/
	if(argc != 2) //Se verifica llamado del programa
	{
		cout << "Uso: "<< argv[0] << " ArchivoDeVideo" << endl;
		return -1; //No se mandaron los comandos correctos
	}

	cv::VideoCapture video(argv[1]); //Intentamos abrir el vídeo
	if(!video.isOpened())
	{
		cout << "Error al abrir archivo de vídeo. Verifique formatos.";	
		return -2; //Por alguna razón, el vídeo no se pudo abrir
	}
	int retFolderVal;
	if( (retFolderVal = mkdir(FOLDERNAME, S_IRWXU | S_IRWXG | S_IRWXO | S_IXOTH)) < 0)
	{
		//Oh oh... Nah, es broma, probablemente la carpeta ya exista.
		if(errno != EEXIST)
		{
			//Oh oh... ahora sí hay problemas.
			return -3;
		}
	}

	/*Fase de inicializaciones*/
	
	cv::Mat actualFrame; //Frame a trabajar en el vídeo
	cv::CascadeClassifier face_detector; //Instancia de detección de rostros
	face_detector.load("haarcascade_frontalface_default.xml");
	vector<cv::Rect> faces;
	int contador = 0; // Contador dedicado a verificar en qué frame se encuentra actualmente el vídeo
	
	/*Fase de procesamiento*/
	video >> actualFrame; //Guardamos en el frame en la Matriz inicializada
	do
	{	
		video >> actualFrame; //Guardamos el frame en la Matriz inicializada
		contador++; //Sube en 1 el contador
		face_detector.detectMultiScale(actualFrame, faces, 1.2, 6, 0, cv::Size(30,30) ); //Se buscan rostros en frame actual
		for(int i = 0; i < faces.size() && contador%10 == 0; i++)
		{
			//Se recortan las imágenes y se guardan en el directorio.		
			cutNSave(&actualFrame, &faces[i], contador, i);
		}
	}
	while(!actualFrame.empty()); //El programa termina cuando todo el video haya sido revisado.

	/*Fase de cierre del programa*/
	actualFrame.release();	
	video.release();
	return 0;
}
